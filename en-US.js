var localizations = {
  /* тестовые метки для ознакомления 
  Задание: 
  1: Накатить плюрализацию на Found # issue,
  2: Поправить опечатку в Clickk the button to continue
  3: Сделать link тегом, а не словом на перевод
  4: Текст ссылки Settings заменить на Options
  5: Сделать переводимыми последние три строки. 
    */
  "test.line.for.plural": "Found # issue",
  "mistake.in.text": "Clickk the button to continue.",
  "add.link.to.text": "Go to link",
  "link.text": "Settings",
  "reorganize.text.2": "Select the purchase",
  "reorganize.text.1": "Select the builder",
  "reorganize.text.0": "component",
};

module.exports = localizations;
