var localizations = {
  /* тестовые метки для ознакомления 
  Задание: 
  1: Накатить плюрализацию на Found # issue,
  2: Поправить опечатку в Clickk the button to continue
  3: Сделать link тегом, а не словом на перевод
  4: Текст ссылки Settings заменить на Options
  5: Сделать переводимыми последние три строки. 
    */
  "test.line.for.plural": "Найдено # проблема",
  "mistake.in.text": "Нажмите на кнопку, чтобы продолжить.",
  "add.link.to.text": "Перейти по ссылке",
  "link.text": "Настройки",
  "reorganize.text.2": "Выберите покупку",
  "reorganize.text.1": "Выберите строителя",
  "reorganize.text.0": "компонент",
};

module.exports = localizations;
